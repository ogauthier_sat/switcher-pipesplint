/*
 * This file is part of switcher-plugin-example.
 *
 * switcher-plugin-example is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#undef NDEBUG  // get assert in release mode

#include <thread>
#include <cassert>
#include <chrono>

#include "switcher/quiddity/basic-test.hpp"
#include "switcher/switcher.hpp"

int main() {
  using namespace switcher;
  using namespace quiddity;

  // create a instance of Switcher
  Switcher::ptr manager = Switcher::make_switcher("test_manager", true);
  
  // run full test on quiddity
  //assert(quiddity::test::full(manager, "pipesplint"));

  // instanciate quiddity videotestsrc
  auto videoTestSrc1_qrox = manager->quiddities->create("videotestsrc", "videotestsrc1", nullptr);
  auto videoTestSrc1_quid = videoTestSrc1_qrox.get();
  assert(videoTestSrc1_quid);

  // start "videoTestSrc" quiddity
  videoTestSrc1_quid->prop<MPtr(&property::PBag::set_str_str)>("started", "true");

  // instanciate quiddity audiotestsrc
  auto audioTestSrc1_qrox = manager->quiddities->create("audiotestsrc", "audiotestsrc1", nullptr);
  auto audioTestSrc1_quid = audioTestSrc1_qrox.get();
  assert(audioTestSrc1_quid);

  // start "audioTestSrc" quiddity
  audioTestSrc1_quid->prop<MPtr(&property::PBag::set_str_str)>("started", "true");


  // instanciate quiddity - webrtcOutput configuration (shmdatasrc -> webrtcsink)
  auto webrtcOutput1_qrox = manager->quiddities->create("pipesplint", "webrtcOutput1", nullptr);
  auto webrtcOutput1_quid = webrtcOutput1_qrox.get();
  assert(webrtcOutput1_quid);

  // select "webrtcOutput" configuration
  webrtcOutput1_quid->prop<MPtr(&property::PBag::set_str_str)>("pipeline_config", "webrtcAudioOutput");

  // connect "webrtcOutput1" to "videoTestsrc1"
  // assert(webrtcOutput1_qrox.get()->claw<MPtr(&quiddity::claw::Claw::try_connect)>(videoTestSrc1_quid->get_id()));
  // connecting a video shmdatasrc to webrtcsink causes stream not to start
  // videorate gstvideorate.c:1955:gst_video_rate_transform_ip:<videorate2> Got buffer with GST_CLOCK_TIME_NONE timestamp, discarding it
  // webrtcsink net/webrtc/src/webrtcsink/imp.rs:3326:gstrswebrtc::webrtcsink::imp::BaseWebRTCSink::feed_discoveries:<appsrc0> Failed to push buffer: Pad is flushing
  // webrtcsink net/webrtc/src/webrtcsink/imp.rs:3326:gstrswebrtc::webrtcsink::imp::BaseWebRTCSink::feed_discoveries:<appsrc2> Failed to push buffer: Pad is flushing

  // connect "webrtcOutput1" to "audioTestsrc1"
  assert(webrtcOutput1_qrox.get()->claw<MPtr(&quiddity::claw::Claw::try_connect)>(audioTestSrc1_quid->get_id()));

  // wait 1s
  std::this_thread::sleep_for(std::chrono::milliseconds(1000));


  // start "webrtcOutput1" quiddity
  webrtcOutput1_quid->prop<MPtr(&property::PBag::set_str_str)>("started", "true");

  // wait 10s
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));

  return 0;
}
