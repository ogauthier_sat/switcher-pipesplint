/*
 * This file is part of switcher-plugin-example.
 *
 * switcher-plugin-example is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#ifndef __SWITCHER_PIPESPLINT_PLUGIN_H__
#define __SWITCHER_PIPESPLINT_PLUGIN_H__

#include <memory>
#include <nlohmann/json.hpp>
#include <string>
#include <iterator>
#include <vector>
#include <map>

#include "switcher/gst/pipeliner.hpp"
#include "switcher/gst/unique-gst-element.hpp"
#include "switcher/quiddity/quiddity.hpp"
#include "switcher/quiddity/startable.hpp"
#include "switcher/shmdata/gst-tree-updater.hpp"


// This quiddity implements a simple gst pipeline

using json = nlohmann::json;

namespace switcher {
namespace quiddities {
using namespace quiddity;
class Pipesplint : public Quiddity, public Startable {
 public:
  Pipesplint(quiddity::Config&&);
  Pipesplint() = delete;
  virtual ~Pipesplint();
  Pipesplint(const Pipesplint&) = delete;
  Pipesplint& operator=(const Pipesplint&) = delete;

 private:
  static const std::string kConnectionSpec; //!< Shmdata specifications.
  std::string shmpath_{};                   //!< Shmdata path to read.

  static const std::string default_pipeline_configuration_json_string;

  std::vector<claw::swid_t> shmdata_writers_ids_;

  // GstTreeUpdaters
  std::vector<std::unique_ptr<shmdata::GstTreeUpdater>> shmdata_writers_gst_updaters_{};
  std::map<claw::swid_t, std::unique_ptr<shmdata::GstTreeUpdater>> shmdata_followers_gst_updaters_{};
  std::map<claw::swid_t, GstElement*> sfid_shmdatasrc_{};

  bool on_shmdata_connect(const std::string& shmdata_path, claw::sfid_t sfid);
  bool on_shmdata_disconnect(claw::sfid_t sfid);

  void write_bin_to_dot_file(const std::string suffix);

  gst::Pipe::on_msg_async_cb_t on_msg_async_cb_{nullptr};
  gst::Pipe::on_msg_sync_cb_t on_msg_sync_cb_{nullptr};

  guint bus_watch_id{0};
  GstState target_state_{GST_STATE_PAUSED};
  bool prerolled_{FALSE};
  bool in_progress_{FALSE};
  bool buffering_{FALSE};
  bool is_live_{FALSE};
  bool interrupting_{FALSE};
  bool waiting_eos_{FALSE};
  bool eos_on_shutdown_{FALSE};
  gboolean gst_is_missing_plugin_message (GstMessage* msg);
  const gchar* gst_missing_plugin_message_get_description (GstMessage* msg);
  void do_initial_play();
  void on_msg_async_cb(GstMessage* msg);
  GstBusSyncReply on_msg_sync_cb(GstMessage* msg);



  json pipeline_configuration_json_{};
  json pipeline_properties_json_{};
  std::vector<std::string> pipeline_properties_{};
  std::unique_ptr<gst::Pipeliner> pipeline_{};
  std::vector<std::pair<const std::string, GstElement*>> pipeline_elements_{};

  GstElement* pipesplint_bin_{nullptr};

  std::map<std::string, std::string> configuration_list_path_{};
  std::vector<std::string> make_configuration_list();

  bool set_pipeline_configuration_selection(const quiddity::property::IndexOrName pipeline_configuration_selection);
  const quiddity::property::IndexOrName get_pipeline_configuration_selection();
  quiddity::property::Selection<> pipeline_configuration_selection_;
  quiddity::property::prop_id_t pipeline_configuration_id_;
  quiddity::property::prop_id_t pipeline_properties_group_id_;

  bool parse_json();
  void remove_pipeline_properties();
  void expose_object_property(GObject* object, const std::string property_name);
  void set_property_value_from_json(GObject* object, const std::string property_name, const nlohmann::json value_json);
  void expose_element_properties(std::pair<const std::string, GstElement*> element);

  const std::string get_element_factory_name(GstElement* element);

  void update_pipeline_elements();
  void make_pipeline_properties();

  bool attach_follower_claw(const std::string& shmpath, claw::sfid_t sfid);

  bool late_attach_writer_claws_{false};
  void attach_writer_claws();

  void reset();
  bool create();

  bool start() final;
  bool stop() final;
  // bool remake_elements();
};

SWITCHER_DECLARE_PLUGIN(Pipesplint);

}  // namespace quiddities
}  // namespace switcher
#endif
