/*
 * This file is part of switcher-plugin-example.
 *
 * switcher-plugin-example is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#undef NDEBUG  // get assert in release mode

#include <thread>
#include <cassert>
#include <chrono>

#include "switcher/quiddity/basic-test.hpp"
#include "switcher/switcher.hpp"

int main() {
  using namespace switcher;
  using namespace quiddity;

  // create a instance of Switcher
  Switcher::ptr manager = Switcher::make_switcher("test_manager", true);
  
  // run full test on quiddity
  //assert(quiddity::test::full(manager, "pipesplint"));

  // instanciate quiddity - webrtcInput configuration (webrtcsrc -> shmdatasink)
  auto webrtcInput1_qrox = manager->quiddities->create("pipesplint", "webrtcInput1", nullptr);
  auto webrtcInput1_quid = webrtcInput1_qrox.get();
  assert(webrtcInput1_quid);

  // select "webrtcInput" configuration
  webrtcInput1_quid->prop<MPtr(&property::PBag::set_str_str)>("pipeline_config", "webrtcInput");

  // start "webrtcInput1" quiddity
  webrtcInput1_quid->prop<MPtr(&property::PBag::set_str_str)>("started", "true");


  // wait 10s
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));

  return 0;
}
