/*
 * This file is part of switcher-plugin-example.
 *
 * switcher-plugin-example is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#undef NDEBUG  // get assert in release mode

#include <thread>
#include <cassert>
#include <chrono>

#include "switcher/quiddity/basic-test.hpp"
#include "switcher/switcher.hpp"

int main() {
  using namespace switcher;
  using namespace quiddity;

  // create a instance of Switcher
  Switcher::ptr manager = Switcher::make_switcher("test_manager", true);
  
  // run full test on quiddity
  //assert(quiddity::test::full(manager, "pipesplint"));


  // instanciate quiddity - built-in configurations
  auto pipesplint_builtin_qrox = manager->quiddities->create("pipesplint", "pipesplint_builtin", nullptr);
  auto pipesplint_builtin_quid = pipesplint_builtin_qrox.get();
  assert(pipesplint_builtin_quid);

  // select no configuration
  pipesplint_builtin_quid->prop<MPtr(&property::PBag::set_str_str)>("pipeline_config", "0");

  // select default configuration
  pipesplint_builtin_quid->prop<MPtr(&property::PBag::set_str_str)>("pipeline_config", "1");

  // start quiddity
  pipesplint_builtin_quid->prop<MPtr(&property::PBag::set_str_str)>("started", "true");

  // stop quiddity
  pipesplint_builtin_quid->prop<MPtr(&property::PBag::set_str_str)>("started", "false");

  // remove quiddity
  manager->quiddities->remove(pipesplint_builtin_quid->get_id());

  // wait 1s
  std::this_thread::sleep_for(std::chrono::milliseconds(1000));


  // instanciate quiddity - videoTestInput configuration (videotestsrc -> shmdatasink)
  auto pipesplint_videoTestInput1_qrox = manager->quiddities->create("pipesplint", "pipesplint_videoTestInput1", nullptr);
  auto pipesplint_videoTestInput1_quid = pipesplint_videoTestInput1_qrox.get();
  assert(pipesplint_videoTestInput1_quid);

  // select "videoTestInput" configuration
  pipesplint_videoTestInput1_quid->prop<MPtr(&property::PBag::set_str_str)>("pipeline_config", "videoTestInput");

  // start "videoTestInput" quiddity
  pipesplint_videoTestInput1_quid->prop<MPtr(&property::PBag::set_str_str)>("started", "true");

  // wait 1s
  std::this_thread::sleep_for(std::chrono::milliseconds(1000));


  // instanciate quiddity - videoOutput configuration (shmdatasrc -> autovideosink)
  auto pipesplint_videoOutput1_qrox = manager->quiddities->create("pipesplint", "pipesplint_videoOutput1", nullptr);
  auto pipesplint_videoOutput1_quid = pipesplint_videoOutput1_qrox.get();
  assert(pipesplint_videoOutput1_quid);

  // select "videoOutput" configuration
  pipesplint_videoOutput1_quid->prop<MPtr(&property::PBag::set_str_str)>("pipeline_config", "videoOutput");

  // connect "videoTestInput1" to "videoOutput1"
  auto videoTestInput1_videoOutput1_sfid = pipesplint_videoOutput1_qrox.get()->claw<MPtr(&quiddity::claw::Claw::try_connect)>(pipesplint_videoTestInput1_quid->get_id());
  assert(videoTestInput1_videoOutput1_sfid);

  // start "videoOutput1" quiddity
  pipesplint_videoOutput1_quid->prop<MPtr(&property::PBag::set_str_str)>("started", "true");

  // wait 3s
  std::this_thread::sleep_for(std::chrono::milliseconds(3000));

  // disconnect "videoTestInput1" from "videoOutput1"
  std::cout << "DISCONNECTING!!!!!!" << std::endl;
  pipesplint_videoOutput1_qrox.get()->claw<MPtr(&quiddity::claw::Claw::disconnect)>(videoTestInput1_videoOutput1_sfid);

  // wait 2s
  std::this_thread::sleep_for(std::chrono::milliseconds(2000));

  // reconnect "videoTestInput1" to "videoOutput1"
  videoTestInput1_videoOutput1_sfid = pipesplint_videoOutput1_qrox.get()->claw<MPtr(&quiddity::claw::Claw::try_connect)>(pipesplint_videoTestInput1_quid->get_id());
  assert(videoTestInput1_videoOutput1_sfid);

  // // stop "videoOutput1" quiddity
  // pipesplint_videoOutput1_quid->prop<MPtr(&property::PBag::set_str_str)>("started", "false");

  // // wait 1s
  // std::this_thread::sleep_for(std::chrono::milliseconds(1000));

  // // start "videoOutput1" quiddity
  // pipesplint_videoOutput1_quid->prop<MPtr(&property::PBag::set_str_str)>("started", "true");

  // wait 10s
  std::this_thread::sleep_for(std::chrono::milliseconds(10000));


/* 
  // instanciate quiddity - passthrough configuration (shmdatasrc -> shmdatasink)
  auto pipesplint_passthrough1_qrox = manager->quiddities->create("pipesplint", "pipesplint_passthrough1", nullptr);
  auto pipesplint_passthrough1_quid = pipesplint_passthrough1_qrox.get();
  assert(pipesplint_passthrough1_quid);

  // select "passthrough" configuration
  pipesplint_passthrough1_quid->prop<MPtr(&property::PBag::set_str_str)>("pipeline_config", "passthrough");

  // connect "videoTestInput1" to "passthrough1"
  assert(pipesplint_passthrough1_qrox.get()->claw<MPtr(&quiddity::claw::Claw::try_connect)>(pipesplint_videoTestInput1_quid->get_id()));

  // start "passthrough1" quiddity
  pipesplint_passthrough1_quid->prop<MPtr(&property::PBag::set_str_str)>("started", "true");

  // wait 1s
  std::this_thread::sleep_for(std::chrono::milliseconds(1000));

 */
  return 0;
}
