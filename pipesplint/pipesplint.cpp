/*
 * This file is part of switcher-plugin-example.
 *
 * switcher-plugin-example is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General
 * Public License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place, Suite 330,
 * Boston, MA 02111-1307, USA.
 */

#include "pipesplint.hpp"

#include <iostream>
#include <fstream>

#include "switcher/quiddity/property/gprop-to-prop.hpp"
#include "switcher/utils/scope-exit.hpp"
#include "switcher/shmdata/caps/utils.hpp"

namespace switcher {
namespace quiddities {
SWITCHER_MAKE_QUIDDITY_DOCUMENTATION(Pipesplint,
                                     "pipesplint",
                                     "Dynamic GStreamer pipeline",
                                     "Manage dynamic GStreamer pipelines defined from a JSON file",
                                     "LGPL",
                                     "Olivier Gauthier");

#define READONLY_TOKEN "_READONLY"

const std::string Pipesplint::kConnectionSpec(R"(
{
"follower":
  [
    {
      "label": "video",
      "description": "Video stream",
      "can_do": ["video/x-raw"]
    },
    {
      "label": "audio",
      "description": "Audio stream",
      "can_do": ["audio/x-raw"]
    }
  ],
"writer":
  [
    {
      "label": "video%",
      "description": "Video stream",
      "can_do": ["video/x-raw"]
    },
    {
      "label": "audio%",
      "description": "Audio stream",
      "can_do": ["audio/x-raw"]
    }
  ]
}
)");

const std::string Pipesplint::default_pipeline_configuration_json_string(R"(
{
  "pipeline_name": "passthrough",
  "pipeline_description": "shmdatasrc name=shmdatasrc0 ! shmdatasink name=shmdatasink0",
  "pipeline_properties": {
    "shmdatasrc0": {
      "socket-path": "/tmp/pipesplint_passthrough_input",
      "copy-buffers": false,
      "blocksize": 4096,
      "caps": "_READONLY",
      "connected": "_READONLY",
      "buffers": "_READONLY",
      "bytes": "_READONLY"
    },
    "shmdatasink0": {
      "socket-path": "/tmp/pipesplint_passthrough_output",
      "copy-buffers": false,
      "blocksize": 4096,
      "enable-last-sample": true,
      "sync": true,
      "caps": "_READONLY",
      "connected": "_READONLY",
      "buffers": "_READONLY",
      "bytes": "_READONLY",
      "stats": "_READONLY"
    }
  }
}
)");

Pipesplint::Pipesplint(quiddity::Config&& conf)
    : Quiddity(std::forward<quiddity::Config>(conf),
                {kConnectionSpec,
                  [this](const std::string& shmpath, claw::sfid_t sfid) { return on_shmdata_connect(shmpath, sfid); },
                  [this](claw::sfid_t sfid) { return on_shmdata_disconnect(sfid); }}),
      quiddity::Startable(this),
      on_msg_async_cb_([this](GstMessage* msg) { this->on_msg_async_cb(msg); }),
      on_msg_sync_cb_([this](GstMessage* msg) { return this->on_msg_sync_cb(msg); }),
      pipeline_(std::make_unique<gst::Pipeliner>(on_msg_async_cb_, on_msg_sync_cb_)),
      pipeline_configuration_selection_(
        make_configuration_list(),
        0
      ) {
  sw_debug("Pipesplint::Pipesplint constructor");

  if (!static_cast<bool>(pipeline_.get())) {
    sw_error("Pipesplint::Pipesplint: Pipeline not found");
  }

  // configuration id property to select which configuration to load
  pipeline_configuration_id_ = pmanage<MPtr(&quiddity::property::PBag::make_selection<>)>(
    "pipeline_config",
    [this](const quiddity::property::IndexOrName& pipeline_configuration_selection) {
      // configuration selection getter
      return set_pipeline_configuration_selection(pipeline_configuration_selection);
    },
    [this]() {
      // configuration selection setter
      return get_pipeline_configuration_selection();
    },
    "Pipeline configuration",
    "Select pipeline configuration",
    pipeline_configuration_selection_
  );

  // property group to hold gstreamer elements properties
  pipeline_properties_group_id_ = pmanage<MPtr(&property::PBag::make_group)>(
    "pipeline_properties",
    "Pipeline properties",
    "Properties mapped to GStreamer pipeline"
  );


}


Pipesplint::~Pipesplint() {
  sw_debug("Pipesplint::Pipesplint destructor");
  gst_element_send_event (pipeline_->get_pipeline(), gst_event_new_eos ());
}

bool Pipesplint::on_shmdata_disconnect(claw::sfid_t sfid) {
  if (shmdata_followers_gst_updaters_.empty()) {
    return false;
  }

  if (shmdata_followers_gst_updaters_.erase(sfid) == 1) {
    GstElement* shmdatasrc_element = sfid_shmdatasrc_.at(sfid);
    // set empty shmpath in shmdatasrc element socket-path property
    g_object_set(G_OBJECT(shmdatasrc_element), "socket-path", "", nullptr);

    // remove shmdatasrc element
    sfid_shmdatasrc_.erase(sfid);

    write_bin_to_dot_file("FOLLOWER_DETACHED");
    
    sw_debug("Pipesplint::on_shmdata_disconnect: detached follower for sfid '{}'", sfid);
    return true;
  } else {
    return false;
  }
}

bool Pipesplint::on_shmdata_connect(const std::string& shmpath, claw::sfid_t sfid) {
  sw_debug("Pipesplint::on_shmdata_connect: Connecting shmpath '{}' for sfid '{}'", shmpath, sfid);
  
  if(shmpath.empty()) {
    sw_error("Pipesplint::on_shmdata_connect: Shmpath is empty, won't connect follower");
    return false;
  }

  return attach_follower_claw(shmpath, sfid);
}

void Pipesplint::write_bin_to_dot_file(const std::string suffix){
  // call program with environment variable GST_DEBUG_DUMP_DOT_DIR set
  // i.e.: GST_DEBUG_DUMP_DOT_DIR="/tmp" wrappers/switcherio/server.py --debug

  const std::string prefix = "pipesplint";
  const std::string pipesplint_quiddity_nickname = get_nickname();
  const std::string pipeline_name = pipeline_configuration_json_["pipeline_name"];
  const std::string dot_file_name = prefix + "_" + pipesplint_quiddity_nickname + "_" + pipeline_name + "_" + suffix;

  GST_DEBUG_BIN_TO_DOT_FILE(GST_BIN(pipeline_->get_pipeline()), GST_DEBUG_GRAPH_SHOW_ALL, dot_file_name.c_str());

  if(const char* gst_debug_dump_dot_dir = std::getenv("GST_DEBUG_DUMP_DOT_DIR")) {
    const std::string dot_file_dir{gst_debug_dump_dot_dir};
    const std::string dot_file_path = dot_file_dir + "/" + dot_file_name;
    sw_debug("Pipesplint::write_bin_to_dot_file: written dot file {}", dot_file_path);
  }

  // std::string dot{gst_debug_bin_to_dot_data(GST_BIN(pipesplint_bin_), GST_DEBUG_GRAPH_SHOW_ALL)};
  // sw_debug("Pipesplint::write_bin_to_dot_file: \n{}", dot);
}

/* Kids, use the functions from libgstpbutils in gst-plugins-base in your
 * own code (we can't do that here because it would introduce a circular
 * dependency) */
gboolean Pipesplint::gst_is_missing_plugin_message(GstMessage* msg) {
  if (GST_MESSAGE_TYPE (msg) != GST_MESSAGE_ELEMENT
      || gst_message_get_structure (msg) == NULL)
    return FALSE;

  return gst_structure_has_name (gst_message_get_structure (msg),
      "missing-plugin");
}

const gchar* Pipesplint::gst_missing_plugin_message_get_description(GstMessage* msg) {
  return gst_structure_get_string (gst_message_get_structure (msg), "name");
}

void Pipesplint::do_initial_play() {
  // PRINT (_("Setting pipeline to PLAYING ...\n"));
  sw_debug("Pipesplint::do_initial_play: Setting pipeline to PLAYING ...");

  // tfthen = gst_util_get_timestamp ();

  if (gst_element_set_state (pipeline_->get_pipeline(),
          GST_STATE_PLAYING) == GST_STATE_CHANGE_FAILURE) {
    // gst_printerr (_("ERROR: pipeline doesn't want to play.\n"));
    sw_error("Pipesplint::do_initial_play: ERROR: pipeline doesn't want to play.");
    // last_launch_code = LEC_STATE_CHANGE_FAILURE;

    /* error message will be posted later */
    return;
  }

  target_state_ = GST_STATE_PLAYING;
}


GstBusSyncReply Pipesplint::on_msg_sync_cb(GstMessage* message) {

  switch (GST_MESSAGE_TYPE (message)) {
    case GST_MESSAGE_STATE_CHANGED:
      /* we only care about pipeline state change messages */
      if (GST_MESSAGE_SRC (message) == GST_OBJECT_CAST (pipeline_->get_pipeline())) {
        GstState old_state, new_state, pending_state;
        gchar *state_transition_name;

        gst_message_parse_state_changed (message, &old_state, &new_state, &pending_state);

        state_transition_name = g_strdup_printf ("%s_%s",
            gst_element_state_get_name (old_state), gst_element_state_get_name (new_state));

        /* dump graph for (some) pipeline state changes */
        // {
          // gchar *dump_name = g_strconcat ("gst-launch.", state_transition_name,
          //     NULL);
          // GST_DEBUG_BIN_TO_DOT_FILE_WITH_TS (GST_BIN (pipeline),
          //     GST_DEBUG_GRAPH_SHOW_ALL, dump_name);

          // g_free (dump_name);
        // }

        sw_debug("Pipesplint::on_msg_sync_cb: Pipeline transitionned {}", state_transition_name);
        write_bin_to_dot_file(state_transition_name);

        // /* place a marker into e.g. strace logs */
        // {
        //   gchar *access_name = g_strconcat (g_get_tmp_dir (), G_DIR_SEPARATOR_S,
        //       "gst-launch", G_DIR_SEPARATOR_S, state_transition_name, NULL);
        //   g_file_test (access_name, G_FILE_TEST_EXISTS);
        //   g_free (access_name);
        // }

        g_free (state_transition_name);
      }
      break;
    case GST_MESSAGE_ERROR:{
      /* dump graph on error */
      // GST_DEBUG_BIN_TO_DOT_FILE_WITH_TS (GST_BIN (pipeline),
      //     GST_DEBUG_GRAPH_SHOW_ALL, "gst-launch.error");
      write_bin_to_dot_file("error");

      // print_error_message (message);

      GError *err = nullptr;
      gchar *name, *debug = nullptr;

      name = gst_object_get_path_string (message->src);
      gst_message_parse_error (message, &err, &debug);

      // gst_printerr (_("ERROR: from element %s: %s\n"), name, err->message);
      sw_error("Pipesplint::on_msg_sync_cb: ERROR: from element {}: {}", name, err->message);

      if (debug != nullptr) {
        // gst_printerr (_("Additional debug info:\n%s\n"), debug);
        sw_error("Pipesplint::on_msg_sync_cb: Additional debug info:\n {}", debug);
      }

      g_clear_error (&err);
      g_free (debug);
      g_free (name);

      if (target_state_ == GST_STATE_PAUSED) {
        // gst_printerr (_("ERROR: pipeline doesn't want to preroll.\n"));
        sw_error("Pipesplint::on_msg_sync_cb: ERROR: pipeline doesn't want to preroll.");
      } else if (interrupting_) {
        // PRINT (_("An error happened while waiting for EOS\n"));
        sw_error("Pipesplint::on_msg_sync_cb: An error happened while waiting for EOS");
      }

      /* we have an error */
      // last_launch_code = LEC_ERROR;
      // g_main_loop_quit (loop);
      break;
    }
    default:
      break;
  }
  return GST_BUS_PASS;
}

void Pipesplint::on_msg_async_cb(GstMessage* message) {

  // debug messages
  bool messages = true;
  if (messages) {
    GstObject *src_obj;
    const GstStructure *s;
    guint32 seqnum;

    seqnum = gst_message_get_seqnum (message);

    s = gst_message_get_structure (message);

    src_obj = GST_MESSAGE_SRC (message);


    std::string message_debug{};
  
    if (GST_IS_ELEMENT (src_obj)) {
      // PRINT (_("Got message #%u from element \"%s\" (%s): "),
      //     (guint) seqnum, GST_ELEMENT_NAME (src_obj),
      //     GST_MESSAGE_TYPE_NAME (message));
      message_debug += " from element " + std::string{GST_ELEMENT_NAME (src_obj)} + " " + GST_MESSAGE_TYPE_NAME (message) + ": ";
    } else if (GST_IS_PAD (src_obj)) {
      // PRINT (_("Got message #%u from pad \"%s:%s\" (%s): "),
      //     (guint) seqnum, GST_DEBUG_PAD_NAME (src_obj),
      //     GST_MESSAGE_TYPE_NAME (message));
      message_debug += " from pad " + std::string{GST_PAD_NAME (src_obj)} + " " + GST_MESSAGE_TYPE_NAME (message) + ": ";
    } else if (GST_IS_OBJECT (src_obj)) {
      // PRINT (_("Got message #%u from object \"%s\" (%s): "),
      //     (guint) seqnum, GST_OBJECT_NAME (src_obj),
      //     GST_MESSAGE_TYPE_NAME (message));
      message_debug += " from object " + std::string{GST_OBJECT_NAME (src_obj)} + " " + GST_MESSAGE_TYPE_NAME (message) + ": ";
    } else {
      // PRINT (_("Got message #%u (%s): "), (guint) seqnum,
      //     GST_MESSAGE_TYPE_NAME (message));
      message_debug += " from object " + std::string{GST_MESSAGE_TYPE_NAME (message)} + ": ";
    }

    if (s) {
      gchar *sstr;

      sstr = gst_structure_to_string (s);
      // PRINT ("%s\n", sstr);
      message_debug += sstr;

      g_free (sstr);
    } else {
      // PRINT ("no message details\n");
      message_debug += "no message details";
    }

    sw_debug("Pipesplint::on_msg_async_cb: Got message #{} {}", std::to_string(seqnum), message_debug);
  }

  // handle message

  switch (GST_MESSAGE_TYPE (message)) {
    case GST_MESSAGE_NEW_CLOCK:
    {
      GstClock *clock;

      gst_message_parse_new_clock (message, &clock);

      // PRINT ("New clock: %s\n", (clock ? GST_OBJECT_NAME (clock) : "NULL"));
      std::string clock_name{(clock ? GST_OBJECT_NAME (clock) : "NULL")};
      sw_debug("Pipesplint::on_msg_async_cb: New clock: {}", clock_name);
      break;
    }
    case GST_MESSAGE_CLOCK_LOST:
      // PRINT ("Clock lost, selecting a new one\n");
      sw_debug("Pipesplint::on_msg_async_cb: Clock lost, selecting a new one");
      gst_element_set_state (pipeline_->get_pipeline(), GST_STATE_PAUSED);
      gst_element_set_state (pipeline_->get_pipeline(), GST_STATE_PLAYING);
      break;
    case GST_MESSAGE_EOS:{
      // PRINT (_("Got EOS from element \"%s\".\n"),
      //     GST_MESSAGE_SRC_NAME (message));

      // if (eos_on_shutdown)
      //   PRINT (_("EOS received - stopping pipeline...\n"));
      // g_main_loop_quit (loop);
      sw_debug("Pipesplint::on_msg_async_cb: Got EOS from element '{}'", GST_MESSAGE_SRC_NAME (message));
      break;
    }
    case GST_MESSAGE_TAG:
      // if (tags) {
      //   GstTagList *tag_list;

      //   if (GST_IS_ELEMENT (GST_MESSAGE_SRC (message))) {
      //     PRINT (_("FOUND TAG      : found by element \"%s\".\n"),
      //         GST_MESSAGE_SRC_NAME (message));
      //   } else if (GST_IS_PAD (GST_MESSAGE_SRC (message))) {
      //     PRINT (_("FOUND TAG      : found by pad \"%s:%s\".\n"),
      //         GST_DEBUG_PAD_NAME (GST_MESSAGE_SRC (message)));
      //   } else if (GST_IS_OBJECT (GST_MESSAGE_SRC (message))) {
      //     PRINT (_("FOUND TAG      : found by object \"%s\".\n"),
      //         GST_MESSAGE_SRC_NAME (message));
      //   } else {
      //     PRINT (_("FOUND TAG\n"));
      //   }

      //   gst_message_parse_tag (message, &tag_list);
      //   gst_tag_list_foreach (tag_list, print_tag, NULL);
      //   gst_tag_list_unref (tag_list);
      // }
      break;
    case GST_MESSAGE_TOC:
      // if (toc) {
      //   GstToc *toc;
      //   GList *entries;
      //   gboolean updated;

      //   if (GST_IS_ELEMENT (GST_MESSAGE_SRC (message))) {
      //     PRINT (_("FOUND TOC      : found by element \"%s\".\n"),
      //         GST_MESSAGE_SRC_NAME (message));
      //   } else if (GST_IS_OBJECT (GST_MESSAGE_SRC (message))) {
      //     PRINT (_("FOUND TOC      : found by object \"%s\".\n"),
      //         GST_MESSAGE_SRC_NAME (message));
      //   } else {
      //     PRINT (_("FOUND TOC\n"));
      //   }

      //   gst_message_parse_toc (message, &toc, &updated);
      //   /* recursively loop over toc entries */
      //   entries = gst_toc_get_entries (toc);
      //   g_list_foreach (entries, print_toc_entry, GUINT_TO_POINTER (0));
      //   gst_toc_unref (toc);
      // }
      break;
    case GST_MESSAGE_INFO:{
      GError *gerror;
      gchar *debug;
      gchar *name = gst_object_get_path_string (GST_MESSAGE_SRC (message));

      gst_message_parse_info (message, &gerror, &debug);
      if (debug) {
        // PRINT (_("INFO:\n%s\n"), debug);
        sw_debug("Pipesplint::on_msg_async_cb: INFO from element '{}': {}", name, debug);
      }
      
      g_clear_error (&gerror);
      g_free (debug);
      g_free (name);
      break;
    }
    case GST_MESSAGE_WARNING:{
      GError *gerror;
      gchar *debug;
      gchar *name = gst_object_get_path_string (GST_MESSAGE_SRC (message));

      /* dump graph on warning */
      // GST_DEBUG_BIN_TO_DOT_FILE_WITH_TS (GST_BIN (pipeline),
      //     GST_DEBUG_GRAPH_SHOW_ALL, "gst-launch.warning");
      write_bin_to_dot_file("warning");

      gst_message_parse_warning (message, &gerror, &debug);
      // PRINT (_("WARNING: from element %s: %s\n"), name, gerror->message);
      sw_debug("Pipesplint::on_msg_async_cb: INFO from object '{}': {}", name, gerror->message);
      if (debug) {
        // PRINT (_("Additional debug info:\n%s\n"), debug);
        sw_debug("Pipesplint::on_msg_async_cb: Additional debug info: {}", debug);
      }
      g_clear_error (&gerror);
      g_free (debug);
      g_free (name);
      break;
    }
    case GST_MESSAGE_STATE_CHANGED:{
      GstState old_state, new_state, pending_state;

      /* we only care about pipeline state change messages */
      if (GST_MESSAGE_SRC (message) != GST_OBJECT_CAST (pipeline_->get_pipeline()))
        break;

      gst_message_parse_state_changed (message, &old_state, &new_state, &pending_state);

      if (target_state_ == GST_STATE_PAUSED && new_state == target_state_) {
        prerolled_ = TRUE;

        // PRINT (_("Pipeline is PREROLLED ...\n"));
        sw_debug("Pipesplint::on_msg_async_cb: Pipeline is PREROLLED ...");

        write_bin_to_dot_file("PREROLLED");
        if(late_attach_writer_claws_) {
          attach_writer_claws();
        }

        /* ignore when we are buffering since then we mess with the states
          * ourselves. */
        if (buffering_) {
          // PRINT (_("Prerolled, waiting for buffering to finish...\n"));
          sw_debug("Pipesplint::on_msg_async_cb: Prerolled, waiting for buffering to finish...");
          break;
        }
        if (in_progress_) {
          // PRINT (_("Prerolled, waiting for progress to finish...\n"));
          sw_debug("Pipesplint::on_msg_async_cb: Prerolled, waiting for progress to finish...");
          break;
        }

        // do_initial_play (pipeline);
        do_initial_play();
      }
      /* else not an interesting message */
      break;
    }
    case GST_MESSAGE_BUFFERING:{
      gint percent;

      gst_message_parse_buffering (message, &percent);
      //PRINT ("%s %d%%  \r", _("buffering..."), percent);
      sw_debug("Pipesplint::on_msg_async_cb: buffering... {}%", std::to_string(percent));

      /* no state management needed for live pipelines */
      if (is_live_)
        break;

      if (percent == 100) {
        /* a 100% message means buffering is done */
        buffering_ = FALSE;

        if (target_state_ == GST_STATE_PAUSED) {
          // do_initial_play (pipeline);
          do_initial_play();
          break;
        }

        /* if the desired state is playing, go back */
        if (target_state_ == GST_STATE_PLAYING) {
          // PRINT (_("Done buffering, setting pipeline to PLAYING ...\n"));
          sw_debug("Pipesplint::on_msg_async_cb: Done buffering, setting pipeline to PLAYING ...");
          gst_element_set_state (pipeline_->get_pipeline(), GST_STATE_PLAYING);
        }
      } else {
        /* buffering busy */
        if (!buffering_ && target_state_ == GST_STATE_PLAYING) {
          /* we were not buffering but PLAYING, PAUSE  the pipeline. */
          // PRINT (_("Buffering, setting pipeline to PAUSED ...\n"));
          sw_debug("Pipesplint::on_msg_async_cb: Buffering, setting pipeline to PAUSED ...");
          gst_element_set_state (pipeline_->get_pipeline(), GST_STATE_PAUSED);
        }
        buffering_ = TRUE;
      }
      break;
    }
    case GST_MESSAGE_LATENCY:
    {
      // PRINT (_("Redistribute latency...\n"));
      sw_debug("Pipesplint::on_msg_async_cb: Redistribute latency...");
      gst_bin_recalculate_latency (GST_BIN (pipeline_->get_pipeline()));
      break;
    }
    case GST_MESSAGE_REQUEST_STATE:
    {
      GstState state;
      gchar *name = gst_object_get_path_string (GST_MESSAGE_SRC (message));

      gst_message_parse_request_state (message, &state);

      // PRINT (_("Setting state to %s as requested by %s...\n"),
      //     gst_element_state_get_name (state), name);
      sw_debug("Pipesplint::on_msg_async_cb: Setting state to {} as requested by {}...", gst_element_state_get_name (state), name);

      gst_element_set_state (pipeline_->get_pipeline(), state);

      g_free (name);
      break;
    }
    case GST_MESSAGE_APPLICATION:{
      const GstStructure *s;

      s = gst_message_get_structure (message);

      if (gst_structure_has_name (s, "GstLaunchInterrupt")) {
        /* this application message is posted when we caught an interrupt and
          * we need to stop the pipeline. */
        // PRINT (_("Interrupt: Stopping pipeline ...\n"));
        sw_debug("Pipesplint::on_msg_async_cb: Interrupt: Stopping pipeline ...");
        interrupting_ = TRUE;

        if (eos_on_shutdown_) {
          if (waiting_eos_) {
            // PRINT (_
            //     ("Interrupt while waiting for EOS - stopping pipeline...\n"));
            sw_debug("Pipesplint::on_msg_async_cb: Interrupt while waiting for EOS - stopping pipeline...");

            // g_main_loop_quit (loop);
          } else {
            // PRINT (_
            //     ("EOS on shutdown enabled -- Forcing EOS on the pipeline\n"));
            sw_debug("Pipesplint::on_msg_async_cb: EOS on shutdown enabled -- Forcing EOS on the pipeline");
            gst_element_send_event (pipeline_->get_pipeline(), gst_event_new_eos ());

            // PRINT (_("Waiting for EOS...\n"));
            sw_debug("Pipesplint::on_msg_async_cb: Waiting for EOS...");

            waiting_eos_ = TRUE;
          }
        } else {
          // g_main_loop_quit (loop);
        }
      }
      break;
    }
    case GST_MESSAGE_PROGRESS:
    {
      GstProgressType type;
      gchar *code, *text;

      gst_message_parse_progress (message, &type, &code, &text);

      switch (type) {
        case GST_PROGRESS_TYPE_START:
        case GST_PROGRESS_TYPE_CONTINUE:
          in_progress_ = TRUE;
          break;
        case GST_PROGRESS_TYPE_COMPLETE:
        case GST_PROGRESS_TYPE_CANCELED:
        case GST_PROGRESS_TYPE_ERROR:
          in_progress_ = FALSE;
          break;
        default:
          break;
      }
      // PRINT (_("Progress: (%s) %s\n"), code, text);
      sw_debug("Pipesplint::on_msg_async_cb: Progress: ({}) {}", code, text);
      g_free (code);
      g_free (text);

      if (!in_progress_ && prerolled_ && target_state_ == GST_STATE_PAUSED) {
        //do_initial_play (pipeline);
        do_initial_play();
      }
      break;
    }
    case GST_MESSAGE_ELEMENT:{
      if (gst_is_missing_plugin_message (message)) {
        const gchar *desc;

        desc = gst_missing_plugin_message_get_description (message);
        // PRINT (_("Missing element: %s\n"), desc ? desc : "(no description)");
        std::string description_string{desc ? desc : "(no description)"};
        sw_debug("Pipesplint::on_msg_async_cb: Missing element: {}}", description_string);
      }
      break;
    }
    case GST_MESSAGE_HAVE_CONTEXT:{
      GstContext *context;
      const gchar *context_type;
      gchar *context_str;

      gst_message_parse_have_context (message, &context);

      context_type = gst_context_get_context_type (context);
      context_str =
          gst_structure_to_string (gst_context_get_structure (context));
      // PRINT (_("Got context from element '%s': %s=%s\n"),
      //     GST_ELEMENT_NAME (GST_MESSAGE_SRC (message)), context_type,
      //     context_str);
      sw_debug("Pipesplint::on_msg_async_cb: Got context from element '{}': {}={}", GST_ELEMENT_NAME (GST_MESSAGE_SRC (message)), context_type, context_str);
      g_free (context_str);
      gst_context_unref (context);
      break;
    }
    case GST_MESSAGE_PROPERTY_NOTIFY:{
      const GValue *val;
      const gchar *name;
      GstObject *obj;
      gchar *val_str = NULL;
      //gchar **ex_prop, *obj_name;
      gchar *obj_name;

      // if (quiet)
      //   break;

      gst_message_parse_property_notify (message, &obj, &name, &val);

      // /* Let's not print anything for excluded properties... */
      // ex_prop = exclude_args;
      // while (ex_prop != NULL && *ex_prop != NULL) {
      //   if (strcmp (name, *ex_prop) == 0)
      //     break;
      //   ex_prop++;
      // }
      // if (ex_prop != NULL && *ex_prop != NULL)
      //   break;

      obj_name = gst_object_get_path_string (GST_OBJECT (obj));
      if (val != NULL) {
        if (G_VALUE_HOLDS_STRING (val))
          val_str = g_value_dup_string (val);
        else if (G_VALUE_TYPE (val) == GST_TYPE_CAPS)
          val_str = gst_caps_to_string (GST_CAPS(g_value_get_boxed (val)));
        else if (G_VALUE_TYPE (val) == GST_TYPE_TAG_LIST)
          val_str = gst_tag_list_to_string (GST_TAG_LIST(g_value_get_boxed (val)));
        else if (G_VALUE_TYPE (val) == GST_TYPE_STRUCTURE)
          val_str = gst_structure_to_string (GST_STRUCTURE(g_value_get_boxed (val)));
        else
          val_str = gst_value_serialize (val);
      } else {
        val_str = g_strdup ("(no value)");
      }

      gst_print ("%s: %s = %s\n", obj_name, name, val_str);
      sw_debug("Pipesplint::on_msg_async_cb: {}: {} = {}", obj_name, name, val_str);
      g_free (obj_name);
      g_free (val_str);
      break;
    }
    default:
      /* just be quiet by default */
      break;
  }

}

std::vector<std::string> Pipesplint::make_configuration_list() {
  configuration_list_path_.clear();
  std::vector<std::string> configuration_list{};

  // configuration position 0 is reserved for unconfiguration
  configuration_list.emplace_back("none");

  // configuration position 1 is reserved for default configuration
  configuration_list.emplace_back("passthrough");

  // load system configurations
  const std::filesystem::path system_configurations{"/usr/local/share/pipesplint/configurations"};
  for(auto const& configuration_file : std::filesystem::directory_iterator{system_configurations}) {
    const std::string configuration_name = configuration_file.path().filename().stem();
    const std::string configuration_file_path = configuration_file.path();
    sw_debug("Pipesplint::make_configuration_list: Found system configuration '{}': '{}'", configuration_name, configuration_file_path);
    configuration_list.emplace_back(configuration_name);
    configuration_list_path_[configuration_name] = configuration_file_path;
  }

  // load user configurations
  if(const char* xdg_data_home_env = std::getenv("XDG_DATA_HOME")) {
    const std::string xdg_data_home{xdg_data_home_env};
    const std::string user_configurations_path{xdg_data_home + "/pipesplint/configurations"};
    const std::filesystem::path user_configurations{user_configurations_path};
    for(auto const& configuration_file : std::filesystem::directory_iterator{user_configurations}) {
      const std::string configuration_name = configuration_file.path().filename().stem();
      const std::string configuration_file_path = configuration_file.path();
      sw_debug("Pipesplint::make_configuration_list: Found user configuration '{}': '{}'", configuration_name, configuration_file_path);
      configuration_list.emplace_back(configuration_name);
      configuration_list_path_[configuration_name] = configuration_file_path;
    }
  }

  return configuration_list;
}

bool Pipesplint::set_pipeline_configuration_selection(const quiddity::property::IndexOrName pipeline_configuration_selection) {
  sw_debug("Pipesplint::set_pipeline_configuration_selection: Setting configuration '{}' index '{}'", pipeline_configuration_selection.name_, std::to_string(pipeline_configuration_selection.index_));
  pipeline_configuration_selection_.select(pipeline_configuration_selection);

  // Scenic does not invoke reset method to reset quiddity's properties, it sets them to falsy
  // values. Treat selecting first entry 0 ("None" or "") as resetting configuration
  if(pipeline_configuration_selection_.get().index_ == 0 ) {
    reset();
  } else {
    reset();
    parse_json();
    create();
  }

  return true;
}

const switcher::quiddity::property::IndexOrName Pipesplint::get_pipeline_configuration_selection() { return pipeline_configuration_selection_.get(); }

bool Pipesplint::parse_json() {
  // loading default built-in configuration
  if(pipeline_configuration_selection_.get().index_ == 1 ) {
    try {
      pipeline_configuration_json_ = json::parse(default_pipeline_configuration_json_string);
    } catch(const json::parse_error &e) {
      sw_error("Pipesplint::parse_json: JSON configuration parsing error: \nmessage: {}\nexception id: {}\nbyte position of error: {}", e.what(), e.id, e.byte);
      pipeline_properties_json_.clear();
      return false;
    }

    pipeline_properties_json_ = pipeline_configuration_json_["pipeline_properties"];
    return true;
  }
  // loading configuration from file
  else {
    const std::string configuration_name = get_pipeline_configuration_selection().name_;
    const std::string configuration_file_path = configuration_list_path_[configuration_name];
    sw_debug("Pipesplint::parse_json: Parsing configuration '{}': '{}'", configuration_name, configuration_file_path);
    
    std::ifstream configuration_file_stream(configuration_file_path);
    if(!configuration_file_stream.is_open()) {
      sw_error("Pipesplint::parse_json: Could not open configuration file '{}'", configuration_file_path);
      return false;
    }

    try {
      pipeline_configuration_json_ = json::parse(configuration_file_stream);
    } catch(const json::parse_error &e) {
      sw_error("Pipesplint::parse_json: JSON configuration parsing error: \nmessage: {}\nexception id: {}\nbyte position of error: {}", e.what(), e.id, e.byte);
      pipeline_properties_json_.clear();
      return false;
    }

    pipeline_properties_json_ = pipeline_configuration_json_["pipeline_properties"];
    return true;
  }
}

void Pipesplint::remove_pipeline_properties() {
  for (auto& it : pipeline_properties_)
    pmanage<MPtr(&quiddity::property::PBag::remove)>(
        pmanage<MPtr(&quiddity::property::PBag::get_id)>(it));
  pipeline_properties_.clear();
}

void Pipesplint::expose_object_property(GObject* object, const std::string property_name_to_expose) {
    // enumerate object's properties
    guint num_properties = 0;
    GParamSpec** props =
        g_object_class_list_properties(G_OBJECT_GET_CLASS(object), &num_properties);
    On_scope_exit { g_free(props); };

    // iterate object's properties
    for (guint i = 0; i < num_properties; i++) {
      const std::string property_name(g_param_spec_get_name(props[i]));
      // verify if we expose this property
      if(property_name.compare(property_name_to_expose) == 0) {
        // add exposed element property to "pipeline_properties" parent quiddity property
        sw_debug("Pipesplint::expose_object_property: adding property '{}'", property_name);
        pmanage<MPtr(&quiddity::property::PBag::push_parented)>(
            property_name,
            "pipeline_properties",
            quiddity::property::to_prop(object, property_name));
        pipeline_properties_.push_back(property_name);
        sw_debug("Pipesplint::expose_object_property: property '{}' added", property_name);
      }
    }
}

void Pipesplint::set_property_value_from_json(GObject* object, const std::string property_name, const nlohmann::json value_json) {
    // set property value
    if(value_json.is_string()) {
      std::string value = value_json;
      // skip setting value for read-only property
      if(value == READONLY_TOKEN) {
        sw_debug("Pipesplint::expose_element_properties: skipping setting read-only value '{}' to property '{}'", value, property_name);
      } else {
        sw_debug("Pipesplint::expose_element_properties: setting value '{}' to property '{}'", value, property_name);
        gst_util_set_object_arg(object, property_name.c_str(), value.c_str());
      }
    } else if(value_json.is_number_integer()) {
      int value = value_json;
      sw_debug("Pipesplint::expose_element_properties: setting string value '{}' to property '{}'", value, property_name);
      g_object_set(object, property_name.c_str(), value, nullptr);
    } else if(value_json.is_number_float()) {
      double value = value_json;
      sw_debug("Pipesplint::expose_element_properties: setting float value '{}' to property '{}'", value, property_name);
      g_object_set(object, property_name.c_str(), value, nullptr);
    } else if(value_json.is_boolean()) {
      bool value = value_json;
      sw_debug("Pipesplint::expose_element_properties: setting boolean value '{}' to property '{}'", value, property_name);
      g_object_set(object, property_name.c_str(), value, nullptr);
    } else {
      sw_warning("Pipesplint::expose_element_properties: could not set value '{}' to property '{}': value is not string, integer, float or boolean", to_string(value_json), property_name);
      return;
    }
}

void Pipesplint::expose_element_properties(std::pair<const std::string, GstElement*> element) {
  const std::string element_name = element.first;
  GstElement* element_pointer = element.second;

  for(auto& property : pipeline_properties_json_[element_name].items()) {
    const std::string property_name = property.key();
    nlohmann::json value_json = property.value();
    sw_debug("Pipesplint::expose_element_properties: configuration ask to expose property '{}' of element '{}'", property_name, element_name);

    // verify if property name is a children property
    std::size_t start = 0U;
    const std::string children_property_delimiter = "::";
    std::size_t children_property_delimiter_pos = property_name.find(children_property_delimiter);
    if(children_property_delimiter_pos != std::string::npos){
      const std::string children_property_object_name = property_name.substr(start, children_property_delimiter_pos);
      start = children_property_delimiter_pos + children_property_delimiter.length();
      const std::string children_property_name = property_name.substr(start);
      sw_debug("Pipesplint::expose_element_properties: property '{}' is children property of object '{}'", children_property_name, children_property_object_name);

      GObject *children = nullptr;
      if(gst_child_proxy_lookup(GST_CHILD_PROXY(element_pointer), property_name.c_str(), &children, nullptr)) {
        // set default value
        set_property_value_from_json(children, children_property_name, value_json);
        // subscribe to property
        expose_object_property(children, children_property_name);
      } else {
        sw_warning("Pipesplint::expose_element_properties: could not find children object for property '{}' of '{}'", property_name, element_name);
        return;
      }
    } else {
      // set default value
      set_property_value_from_json(G_OBJECT(element_pointer), property_name, value_json);
      // subscribe to property
      expose_object_property(G_OBJECT(element_pointer), property_name);
    }
  }
}

const std::string Pipesplint::get_element_factory_name(GstElement* element) {
  GstBinClass *element_bin_class = GST_BIN_GET_CLASS(element);
  GstElementClass *element_parent_class = &element_bin_class->parent_class;
  GstElementFactory *element_parent_factory = element_parent_class->elementfactory;
  const gchar *element_parent_factory_name = GST_OBJECT_NAME(element_parent_factory);
  return std::string(element_parent_factory_name);
}

void Pipesplint::update_pipeline_elements() {
  // clear old elements
  pipeline_elements_.clear();

  // get pipeline root bin
  GstPipeline *pipeline = GST_PIPELINE(pipeline_->get_pipeline());
  GstBin *pipeline_bin = &pipeline->bin;

  // iterate elements in pipeline
	GstIterator *element_iterator = nullptr;
  On_scope_exit { gst_iterator_free(element_iterator); };
	GValue item = G_VALUE_INIT;
	for(element_iterator = gst_bin_iterate_recurse(pipeline_bin);
    gst_iterator_next(element_iterator, &item) == GST_ITERATOR_OK;
    g_value_reset(&item)) {

    // get element's name
		GstElement *element = GST_ELEMENT_CAST(g_value_get_object(&item));
    const std::string element_name(element->object.name);
    pipeline_elements_.push_back(std::make_pair(element_name, element));
    sw_debug("Pipesplint::update_pipeline_elements: found element '{}' instance of '{}'", element_name, get_element_factory_name(element));
  }
  sw_debug("Pipesplint::update_pipeline_elements: added {} elements", pipeline_elements_.size());
}

void Pipesplint::make_pipeline_properties() {
  remove_pipeline_properties();

  for(auto& element : pipeline_elements_) {
    expose_element_properties(element);
  }
}

bool Pipesplint::attach_follower_claw(const std::string& shmpath, claw::sfid_t sfid) {

  int follower_index = 0;
  // verify if we have shmdatasrc elements
  for(auto& element : pipeline_elements_) {
    const std::string element_factory_name = get_element_factory_name(element.second);
    if(element_factory_name == "shmdatasrc") {
      const std::string element_name = element.first;
      GstElement* shmdatasrc_element = element.second;
      sw_debug("Pipesplint::attach_follower_claw: Found a element '{}' instance of '{}', adding follower claw", element_name, element_factory_name);

      // get shmdatasrc static pad named "src"
      GstPad* shmdatasrc_element_src_pad = gst_element_get_static_pad(shmdatasrc_element, "src");

      // query src pad peer capabilities
      GstCaps* shmdatasrc_element_src_pad_peer_caps = gst_pad_peer_query_caps(shmdatasrc_element_src_pad, nullptr);
      const std::string shmdatasrc_element_src_pad_peer_caps_string{gst_caps_to_string(shmdatasrc_element_src_pad_peer_caps)};
      sw_debug("Pipesplint::attach_follower_claw: Queried '{}' src pad peer caps: {}", element_name, shmdatasrc_element_src_pad_peer_caps_string);

      std::vector<::shmdata::Type> follower_can_dos = claw_.get_follower_can_do(sfid);
      for(auto& follower_can_do : follower_can_dos) {
        const std::string follow_can_do_string = follower_can_do.str();
        if(shmdatasrc_element_src_pad_peer_caps_string.find(follow_can_do_string) != std::string::npos) {
          sw_debug("Pipesplint::attach_follower_claw: Found a match between follow can_do '{}' for sfid '{}' and element '{}' src pad peer caps", follow_can_do_string, std::to_string(sfid), element_name);

          // set shmpath in shmdatasrc element socket-path property
          g_object_set(G_OBJECT(shmdatasrc_element), "socket-path", shmpath.c_str(), nullptr);

          // store shmdatasrc element with its sfid
          sfid_shmdatasrc_.emplace(sfid, shmdatasrc_element);

          // use shmdata::GstTreeUpdater to notify clients of new connection
          // store GstTreeUpdater using shmdata follower id
          shmdata_followers_gst_updaters_.emplace(sfid, std::make_unique<shmdata::GstTreeUpdater>(
            this, shmdatasrc_element, shmpath, sfid));

          //gst_element_set_state(shmdatasrc_element, GST_STATE_READY);
          gst_element_sync_state_with_parent(shmdatasrc_element);
         
          sw_debug("Pipesplint::attach_follower_claw: attached '{}' follower to '{}' for sfid '{}'", shmpath, element_name, sfid);
          follower_index++;
        }
      }

    }
    if(follower_index != 0 ) {
      write_bin_to_dot_file("FOLLOWER_ATTACHED");
      return true;
    }
  }

  sw_warning("Pipesplint::attach_follower_claw: No 'shmdatasrc' element to attach follower shmdata. Did you add a least one 'shmdatasrc' element in your pipeline?");
  return false;
}

void Pipesplint::attach_writer_claws() {

  int writer_index = 0;
  // verify if we have shmdatasink elements
  for(auto& element : pipeline_elements_) {
    const std::string element_factory_name = get_element_factory_name(element.second);
    if(element_factory_name == "shmdatasink") {
      writer_index++;
      const std::string element_name = element.first;
      GstElement* shmdatasink_element = element.second;
      sw_debug("Pipesplint::attach_writer_claws: Found a element '{}' instance of '{}', adding writer claw", element_name, element_factory_name);

      // get shmdatasink static pad named "sink"
      GstPad* shmdatasink_element_sink_pad = gst_element_get_static_pad(shmdatasink_element, "sink");

      // query sink pad peer capabilities
      GstCaps* shmdatasink_element_sink_pad_peer_caps = gst_pad_peer_query_caps(shmdatasink_element_sink_pad, nullptr);
      const std::string shmdatasink_element_sink_pad_peer_caps_string{gst_caps_to_string(shmdatasink_element_sink_pad_peer_caps)};
      sw_debug("Pipesplint::attach_writer_claws: Queried '{}' sink pad peer caps:\n{}", element_name, shmdatasink_element_sink_pad_peer_caps_string);

      // verify if pad peer capabilities returns ANY, means it does not yet have capabilities
      // we flag to re-attach claws when we go in READY state
      if(shmdatasink_element_sink_pad_peer_caps_string == "ANY") {
        late_attach_writer_claws_ = true;
        sw_debug("Pipesplint::attach_writer_claws: Sink pad peer caps is 'ANY', we'll attach claws later");
        return;
      }

      // get sink pad peer capabilities first structure name (aka the media MIME)
      GstStructure* shmdatasink_element_sink_pad_peer_caps_first_structure = 
        gst_caps_get_structure(shmdatasink_element_sink_pad_peer_caps, 0);
      const std::string shmdatasink_element_sink_pad_peer_caps_first_structure_name_string{gst_structure_get_name(shmdatasink_element_sink_pad_peer_caps_first_structure)};
      sw_debug("Pipesplint::attach_writer_claws: '{}' sink pad peer caps first MIME is '{}'", element_name, shmdatasink_element_sink_pad_peer_caps_first_structure_name_string);
      const std::string mime = shmdatasink_element_sink_pad_peer_caps_first_structure_name_string;

      // build writer specification (label/category, description, can_dos (vector of MIMEs, here only one per writer))
      const std::string category = shmdata::caps::get_category(mime);
      const std::string pipeline_name = pipeline_configuration_json_["pipeline_name"];
      const std::string description = pipeline_name + "_" + "mime";
      const claw::shm_spec_t writer_specification = {category, description, {mime}};

      // build shmpath
      // /tmp/pipesplint_<quiddity_nickname>_<pipeline_name>_<writer_index>
      // /tmp/pipesplint_pipesplint1_audioTestInput_1
      const std::string pipesplint_quiddity_nickname = get_nickname();
      const std::string pipesplint_quiddity_id = get_id();
      const std::string pipesplint_shmpath_dir = "/tmp";
      const std::string pipesplint_writer_filename = "pipesplint_" + pipesplint_quiddity_nickname + "_" + pipesplint_quiddity_id + "_" + pipeline_name + "_" + std::to_string(writer_index);
      const std::string shmpath = pipesplint_shmpath_dir + "/" + pipesplint_writer_filename;

      // set shmpath in shmdatasink element socket-path property
      g_object_set(G_OBJECT(shmdatasink_element), "socket-path", shmpath.c_str(), nullptr);

      // get meta shmdata writer id from connection spec
      claw::swid_t meta_swid = Ids::kInvalid;
      if(mime == "video/x-raw") {
        meta_swid = claw_.get_swid("video%");
      } else if (mime == "audio/x-raw") {
        meta_swid = claw_.get_swid("audio%");
      } else {
        sw_debug("Pipesplint::attach_writer_claws: Meta writer for other than audio or video is not yet supported");
        return;
      }

      // add shmdata dynamic writer to the meta writer
      sw_debug("Pipesplint::attach_writer_claws: Adding dynamic writer to meta writer '{}'", claw_.get_writer_label(meta_swid));
      claw::swid_t dynamic_swid = claw_.add_writer_to_meta(meta_swid, writer_specification, shmpath);
      
      // take note of new shmdata writer id
      shmdata_writers_ids_.push_back(dynamic_swid);

      // use shmdata::GstTreeUpdater to notify clients of new connection
      shmdata_writers_gst_updaters_.emplace_back(std::make_unique<shmdata::GstTreeUpdater>(
        this, shmdatasink_element, shmpath, shmdata::Direction::writer));

      sw_debug("Pipesplint::attach_writer_claws: attached swid '{} of mime '{}' writer to '{}'", dynamic_swid, mime, shmpath);
    }
  }

}

void Pipesplint::reset() {
  sw_debug("Pipesplint::reset: resetting configuration");

  stop();
  remove_pipeline_properties();

  // remove writers claws
  for (const auto& swid : claw_.get_swids()) {
    claw_.remove_writer_from_meta(swid);
  }

  // remove GstTreeUpdaters, notify clients of connection removal
  shmdata_writers_gst_updaters_.clear();

  sw_debug("Pipesplint::reset: pipesplint_bin_ pointer before reset {}", (uint64_t) pipesplint_bin_);

  if(pipesplint_bin_ != nullptr) {
    if(!gst_bin_remove(GST_BIN(pipeline_->get_pipeline()), pipesplint_bin_)) {
      sw_error("Pipesplint::reset: Could not free bin");
    }
    pipesplint_bin_ = nullptr;
  }

  sw_debug("Pipesplint::reset: pipesplint_bin_ pointer after reset {}", (uint64_t) pipesplint_bin_);

  sw_debug("Pipesplint::reset: configuration resetted");
}

bool Pipesplint::create() {
  reset();

  // validate we have a configuration loaded
  if(pipeline_configuration_json_.empty()) {
    sw_debug("Pipesplint::create: No pipeline configuration loaded, not creating pipeline");
    return false;
  }

  // fetch pipeline description from configuration
  const std::string pipeline_description{pipeline_configuration_json_["pipeline_description"]};
  if(pipeline_description.empty()) {
    sw_warning("Pipesplint::create: No pipeline description available, cannot create pipeline");
    return false;
  }

  // parse pipeline description into pipesplint's bin
  GError* parse_error = nullptr;
  sw_debug("Pipesplint::create: Parsing pipeline description: \n{}", pipeline_description);
  // pipesplint_bin_ = gst_parse_bin_from_description(pipeline_description.c_str(), false, &parse_error);
  pipesplint_bin_ = gst_parse_launch(pipeline_description.c_str(), &parse_error);

  if(!pipesplint_bin_) {
    const std::string parse_error_message(parse_error->message);
    sw_error("Pipesplint::create: Could not parse pipeline description: {}", parse_error_message);
    return false;
  }

  // set pipesplint's bin name to pipeline name
  const std::string pipeline_name{pipeline_configuration_json_["pipeline_name"]};
  gst_object_set_name(GST_OBJECT(pipesplint_bin_), pipeline_name.c_str());

  sw_debug("Pipesplint::create: Pipeline '{}' created", pipeline_name);

  // add pipesplint's bin to root pipeline
  gst_bin_add(GST_BIN(pipeline_->get_pipeline()), pipesplint_bin_);

  write_bin_to_dot_file("CREATED");

  //g_object_set(G_OBJECT(pipeline_->get_pipeline()), "async-handling", FALSE, nullptr);
  //g_object_set(G_OBJECT(pipeline_->get_pipeline()), "async-handling", TRUE, nullptr);

  update_pipeline_elements();

  // attach writer claws and set socket-path on shmdatasinks
  attach_writer_claws();

  // prepare pipeline to paused state
  // with live sources, it will get pipeline to ready state
  //gst_element_set_state(pipeline_->get_pipeline(), GST_STATE_PAUSED);
  // block until GST_STATE_PAUSED/READY state change completes
  // gst::utils::wait_state_changed(pipeline_->get_pipeline());

  make_pipeline_properties();

  sw_debug("Pipesplint::create: Pipeline created");

/*   // preroll pipeline
  // PRINT (_("Setting pipeline to PAUSED ...\n"));
  sw_debug("Pipesplint::create: Setting pipeline to PAUSED ...");
  int ret;
  ret = gst_element_set_state (pipeline_->get_pipeline(), GST_STATE_PAUSED);

  switch (ret) {
    case GST_STATE_CHANGE_FAILURE:
      // gst_printerr (_("Failed to set pipeline to PAUSED.\n"));
      sw_debug("Pipesplint::create: Failed to set pipeline to PAUSED.");
      // last_launch_code = LEC_STATE_CHANGE_FAILURE;
      return false;
    case GST_STATE_CHANGE_NO_PREROLL:
      // PRINT (_("Pipeline is live and does not need PREROLL ...\n"));
      sw_debug("Pipesplint::create: Pipeline is live and does not need PREROLL ...");
      is_live_ = TRUE;
      break;
    case GST_STATE_CHANGE_ASYNC:
      // PRINT (_("Pipeline is PREROLLING ...\n"));
      sw_debug("Pipesplint::create: Pipeline is PREROLLING ...");
      break;
    default:
      break;
  }
 */
  return true;
}

bool Pipesplint::start() {
  if (pipesplint_bin_ == nullptr) {
    sw_warning("Pipesplint::start: Pipeline not initialized, cannot start");
    return false;
  }

  sw_debug("Pipesplint::start: Setting pipeline to PLAYING ...");

  if (gst_element_set_state (pipeline_->get_pipeline(),
          GST_STATE_PLAYING) == GST_STATE_CHANGE_FAILURE) {
    sw_error("Pipesplint::start: ERROR: pipeline doesn't want to play.");
    /* error message will be posted later */
    return false;
  }

  target_state_ = GST_STATE_PLAYING;
  write_bin_to_dot_file("START");

  return true;
}

bool Pipesplint::stop() {
  if (pipesplint_bin_ == nullptr) {
    sw_debug("Pipesplint::stop: Pipeline not initialized, nothing to do.");
    return true;
  }

  sw_debug("Pipesplint::stop: Pipeline stopping");
  //pipeline_->play(false);
  gst_element_set_state(pipeline_->get_pipeline(), GST_STATE_NULL);

  // block until GST_STATE_NULL state change completes
  //gst::utils::wait_state_changed(pipeline_->get_pipeline());
  //sw_debug("Pipesplint::stop: Pipeline stopped");

  write_bin_to_dot_file("STOP");

  return true;
}

}  // namespace quiddities
}  // namespace switcher
