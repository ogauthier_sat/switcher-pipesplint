Switcher pipesplint plugin
========

Dynamic GStreamer pipeline manager.

## Getting Started

```sh
cmake -B build -DCMAKE_BUILD_TYPE=Debug
make -sC build -j$(nproc)
sudo make install -sC build
```

Compile, install and test with GStreamer debugging enabled.

```
make -sC build -j$(nproc) && sudo make install -sC build && GST_DEBUG_DUMP_DOT_DIR="/tmp" GST_DEBUG=1 build/pipesplint/check_pipesplint_plugin
```

Final plugin binary should be installed here `/usr/local/switcher-3.5/plugins/libpipesplint.so`.

Test plugin by running test.

```sh
build/pipesplint/check_pipesplint_plugin
```